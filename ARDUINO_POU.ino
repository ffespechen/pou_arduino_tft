#include <Adafruit_GFX.h>
#include <MCUFRIEND_kbv.h>
MCUFRIEND_kbv tft;
#include <TouchScreen.h>
#define MINPRESSURE 200
#define MAXPRESSURE 1000

// ALL Touch panels and wiring is DIFFERENT
// copy-paste results from TouchScreen_Calibr_native.ino
const int XP=8,XM=A2,YP=A3,YM=9; //320x480 ID=0x9486
const int TS_LEFT=133,TS_RT=914,TS_TOP=949,TS_BOT=90;

TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

const int ANCHO_BOTON = 80;
const int ALTO_BOTON = 80;

#define BLACK 0x0000
#define NAVY 0x000F
#define DARKGREEN 0x03E0
#define DARKCYAN 0x03EF
#define MAROON 0x7800
#define PURPLE 0x780F
#define OLIVE 0x7BE0
#define LIGHTGREY 0xC618
#define DARKGREY 0x7BEF
#define BLUE 0x001F
#define GREEN 0x07E0
#define CYAN 0x07FF
#define RED 0xF800
#define MAGENTA 0xF81F
#define YELLOW 0xFFE0
#define WHITE 0xFFFF
#define ORANGE 0xFD20
#define GREENYELLOW 0xAFE5
#define PINK 0xF81F


#define NORMAL 0
#define DORMIDO 1
#define HAMBRE 2
#define FRIO 3

int vida = 100;
int suenio = 100;
int estado = NORMAL;

/**
 * ------------- FUNCIONES PARA DIBUJAR AL PERSONAJE -------------
 */


//Dibuja el cuerpo del PERSONAJE
void dibujarCuerpo(int color){
  tft.fillCircle(tft.width()/2, tft.height()/2, 120, color);
}

//Ojo Izquierdo
void dibujarOjoIzquierdo(int estado){
    
    if(estado != NORMAL){

      switch(estado){
        case DORMIDO:
          tft.fillCircle(tft.width()/2-ANCHO_BOTON, tft.height()/2-ALTO_BOTON, 20, MAGENTA);
          tft.drawLine(tft.width()/2-ANCHO_BOTON-20, tft.height()/2-ALTO_BOTON, tft.width()/2-ANCHO_BOTON +20, tft.height()/2-ALTO_BOTON, BLACK);
        break;
      }
      
    }else{
      tft.fillCircle(tft.width()/2-ANCHO_BOTON, tft.height()/2-ALTO_BOTON, 20, WHITE);
      tft.drawCircle(tft.width()/2-ANCHO_BOTON, tft.height()/2-ALTO_BOTON, 20, MAGENTA);
      tft.fillCircle(tft.width()/2-ANCHO_BOTON, tft.height()/2-ALTO_BOTON, 5, BLACK);
    }
    
}

//Ojo Derecho
void dibujarOjoDerecho(int estado){


    if(estado != NORMAL){

      switch(estado){
        case DORMIDO:
          tft.fillCircle(tft.width()/2+ANCHO_BOTON, tft.height()/2-ALTO_BOTON, 20, MAGENTA);
          tft.drawLine(tft.width()/2+ANCHO_BOTON-20, tft.height()/2-ALTO_BOTON, tft.width()/2+ANCHO_BOTON +20, tft.height()/2-ALTO_BOTON, BLACK);
        break;
      }

      
    }else{
      tft.fillCircle(tft.width()/2+ANCHO_BOTON, tft.height()/2-ALTO_BOTON, 20, WHITE);
      tft.drawCircle(tft.width()/2+ANCHO_BOTON, tft.height()/2-ALTO_BOTON, 20, MAGENTA);
      tft.fillCircle(tft.width()/2+ANCHO_BOTON, tft.height()/2-ALTO_BOTON, 5, BLACK);
    }
    
}

//Dibuja kos diferentes tipos de bocas del PERSONAJE
void dibujarBoca(int estado){

  if(estado != NORMAL){

    switch(estado){
      case DORMIDO:
       tft.fillCircle(tft.width()/2, tft.height()/2, 10, BLACK);
       delay(500);

      break;
    }
    
  }else{
    tft.fillRoundRect(tft.width()/2-ANCHO_BOTON, tft.height()/2, 2*ANCHO_BOTON, 20, 5, WHITE);
  }
}


/**
 * ------------- FONDO Y ESCENARIOS DEL PERSONAJE -------------
 */


void dibujarFondo(int color){
  tft.fillRect(0, ALTO_BOTON+1, tft.width(), tft.height()-2*ALTO_BOTON, color);
}

//Dibuja el escenario, del POU
void escenario(){
    
    //tft.fillScreen(color);

    //Fondo de botones
    tft.fillRect(0,tft.height()-ALTO_BOTON, tft.width(), ALTO_BOTON, BLACK);


    //Leyenda
    int posY_texto = tft.height() - ALTO_BOTON/2;
    tft.setTextSize(1);
    tft.setTextColor(WHITE);
    
    int separador = 15;
    
    tft.setCursor(separador, posY_texto);
    tft.print("COMIDA");

    tft.setCursor(ANCHO_BOTON+separador, posY_texto);
    tft.print("DORMIR");

    tft.setCursor(2*ANCHO_BOTON+separador, posY_texto);
    tft.print("DUCHA");

    tft.setCursor(3*ANCHO_BOTON+separador, posY_texto);
    tft.print("DESPERTAR");


    //Indicador de vida y sueño
    tft.fillRect(0,0, tft.width(), ALTO_BOTON, BLACK);
    tft.drawRect(0,0, tft.width(), ALTO_BOTON, BLUE);
    tft.setTextColor(GREEN);
    tft.setTextSize(1);
    tft.setCursor(separador, ALTO_BOTON/2 - separador);
    tft.print("Vida: ");
    tft.setCursor(separador, ALTO_BOTON/2 - separador);
    tft.print("Vida: ");
    tft.setCursor(separador, ALTO_BOTON - separador);
    tft.print("Suenio: ");
    tft.setTextColor(WHITE);
    tft.setTextSize(3);
    tft.setCursor(tft.width()/2, ALTO_BOTON/2 - 2*separador);
    tft.print(vida);
    tft.setCursor(tft.width()/2, ALTO_BOTON - 2*separador);
    tft.print(suenio);

}

/**
 * ------------- BOTONES PARA MANEJAR LAS ACCIONES -------------
 */


//Botón Dar Comida
void boton_0_click(){
  Serial.println("Comer");
  delay(1000);
}

//Botón Dormir
void boton_1_click(){
  Serial.println("Dormir");
  estado = DORMIDO;
  dibujarFondo(DARKGREY);
  dibujarCuerpo(MAGENTA);
  dibujarOjoIzquierdo(DORMIDO);
  dibujarOjoDerecho(DORMIDO);
  dibujarBoca(DORMIDO);
  
}

//Botón Baño
void boton_2_click(){
  Serial.println("Bañarse");
}

//Botón Despertar
void boton_3_click(){
  Serial.println("Despertarse");

  if(estado != DORMIDO){
    tft.setCursor(0+10, ALTO_BOTON+10);
    tft.setTextColor(GREEN);
    tft.setTextSize(2);
    tft.print("Hey! Estoy despierto!!");
  }else{
    estado = NORMAL;
    dibujarFondo(NAVY);
    dibujarCuerpo(MAGENTA);
    dibujarOjoIzquierdo(NORMAL);
    dibujarOjoDerecho(NORMAL);
    dibujarBoca(NORMAL);
  }
}

/**
 * Funciones setup y loop
 */

void setup(void)
{
    Serial.begin(9600);
    uint16_t ID = tft.readID();
    tft.begin(ID);
    tft.setRotation(0);            //PORTRAIT

    //Dibujo Inicial del POU
    escenario();
    dibujarCuerpo(MAGENTA);
    dibujarOjoIzquierdo(NORMAL);
    dibujarOjoDerecho(NORMAL);
    dibujarBoca(NORMAL);

}


void loop(void)
{
   TSPoint p = ts.getPoint();
    pinMode(YP, OUTPUT);      //restore shared pins
    pinMode(XM, OUTPUT);
    digitalWrite(YP, HIGH);   //because TFT control pins
    digitalWrite(XM, HIGH);
    
    bool pressed = (p.z > MINPRESSURE && p.z < MAXPRESSURE);
    bool presion_boton = false;
    
    if (pressed) {
        int pixel_x = map(p.x, TS_LEFT, TS_RT, 0, tft.width()); //.kbv makes sense to me
        int pixel_y = map(p.y, TS_TOP, TS_BOT, 0, tft.height());

        int boton = pixel_x/ANCHO_BOTON;
        
        if(pixel_y >= tft.height() - ALTO_BOTON){

          switch (boton)
          {
            case 0:
              boton_0_click();
              break;
            case 1:
              boton_1_click();
              break;
            case 2:
              boton_2_click();
              break;
            case 3:
              boton_3_click();
              break;              
          }
        }

        Serial.print("Pos X : ");
        Serial.print(pixel_x);
        Serial.print("Pos Y : ");
        Serial.println(pixel_y);
        
    
    }

}
